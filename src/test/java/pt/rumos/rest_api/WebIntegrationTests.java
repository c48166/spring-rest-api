package pt.rumos.rest_api;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class WebIntegrationTests {
    
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void indexTest() {

        String out = this.restTemplate.getForObject("http://localhost:" + port + "/", String.class);

        assertAll(() -> assertTrue(out.contains("REST API")),
            () -> assertTrue(out.contains("simulator")),
            () -> assertTrue(out.contains("user")));
    }
}
